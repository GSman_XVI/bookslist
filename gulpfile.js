var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var rename = require('gulp-rename');
var del = require('del');

gulp.task('watch', ['sass'], function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("./src/sass/*.sass", ['sass']);
    gulp.watch("./src/js/*.js", ['ES6']);
    gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
    gulp.watch("./dist/js/*.js").on('change', browserSync.reload);
    gulp.watch("index.html").on('change', browserSync.reload);
});

gulp.task('clean-tmp', function(){
  return del(['dist/tmp']);
});

gulp.task('babel', function(){
  return gulp.src(['src/js/*.js','src/js/**/*.js'])
    .pipe(babel())
    .pipe(gulp.dest('dist/tmp'));
});

gulp.task('ES6', ['babel'], function(){
  return browserify(['dist/tmp/main.js']).bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(rename('app.js'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('sass', function () {
    return gulp.src("./src/sass/*.sass")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("styles.min.css"))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("./dist/css"));
});

gulp.task('bower_libs', function () {
    gulp.src([
        'bootstrap/dist/css/bootstrap.min.css',
        'bootstrap/dist/js/bootstrap.min.js',
        'jquery/dist/jquery.min.js',
    ], { cwd: "bower_components/**" })
        .pipe(gulp.dest("./libs"));
});

gulp.task('default', ['bower_libs', 'sass', 'ES6']);
