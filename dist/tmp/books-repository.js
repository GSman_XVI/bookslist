'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.BooksRepository = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _book = require('./book');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BooksRepository = exports.BooksRepository = function () {
    function BooksRepository() {
        _classCallCheck(this, BooksRepository);

        //Коллекция, в которой будет храниться список книг.
        this.books = new Map();
        this.loadBooks();
    }

    _createClass(BooksRepository, [{
        key: 'getBooks',
        value: function getBooks() {
            return this.books;
        }
    }, {
        key: 'getBook',
        value: function getBook(id) {
            return this.books.get(id);
        }
    }, {
        key: 'addBook',
        value: function addBook(book) {
            //Создаем ключ для новой книги, прибавляя к последнему ключу (если он есть) единицу.
            var lastId = Array.from(this.books.keys()).pop();
            if (isNaN(lastId)) {
                lastId = 0;
            } else {
                lastId++;
            }
            this.books.set(lastId, book);
            this.saveBooks();
        }
    }, {
        key: 'editBook',
        value: function editBook(id, book) {
            this.books.set(id, book);
            this.saveBooks();
        }
    }, {
        key: 'deleteBook',
        value: function deleteBook(id) {
            this.books.delete(id);
            this.saveBooks();
        }
    }, {
        key: 'saveBooks',
        value: function saveBooks() {
            //Преобразуем Map в обычный массив для записи в localStorage.
            var data = [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.books.values()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var book = _step.value;

                    data.push(book);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            localStorage.data = JSON.stringify(data);
        }
    }, {
        key: 'loadBooks',
        value: function loadBooks() {
            //Достаем массив книг из localStorage и записываем их в Map, попутно создавая каждому уникальный ключ.
            if (localStorage.data) {
                var data = JSON.parse(localStorage.data);
                for (var i = 0; i < data.length; i++) {
                    this.books.set(i, data[i]);
                }
            }
        }
    }]);

    return BooksRepository;
}();