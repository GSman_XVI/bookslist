"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Book = exports.Book = function Book(name, author, year, pages) {
    _classCallCheck(this, Book);

    this.name = name;
    this.author = author;
    this.year = year;
    this.pages = pages;
};