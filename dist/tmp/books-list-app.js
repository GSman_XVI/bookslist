'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.BooksListApp = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _book = require('./book');

var _booksRepository = require('./books-repository');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BooksListApp = exports.BooksListApp = function () {
    function BooksListApp() {
        var _this = this;

        _classCallCheck(this, BooksListApp);

        //Вся работа с данными идет через отдельный класс BooksRepository.
        this.books = new _booksRepository.BooksRepository();

        var table = document.querySelector('table');
        table.onclick = function (event) {
            return _this.onBookAction(event);
        };

        var form = document.querySelector('form');
        form.onsubmit = function (event) {
            return _this.onFormSubmit(event);
        };

        this.showBooks();
    }

    _createClass(BooksListApp, [{
        key: 'onBookAction',
        value: function onBookAction(event) {
            //Используя делегирование обрабатываем нажатие на кнопки в списке книг.
            var target = event.target;
            if (target.tagName == 'BUTTON') {
                //В каждом tr присутствует аттрибут data-id, который соответствует идентификатору книги. Определяем его.
                var elemId = +target.parentElement.parentElement.dataset.id;
                if (target.innerHTML == "Edit") {
                    //Вызываем метод, инициирующий редактирование книги
                    this.edit(elemId);
                } else if (target.innerHTML == "Delete") {
                    this.books.deleteBook(elemId);
                    this.showBooks();
                }
            }
        }
    }, {
        key: 'onFormSubmit',
        value: function onFormSubmit(event) {
            var action = document.querySelector('#submit');
            var form = document.forms.book;
            var book = new _book.Book(form.name.value, form.author.value, form.year.value, form.pages.value);
            if (action.innerHTML == 'Add') {
                this.books.addBook(book);
                this.showBooks();
            } else if (action.innerHTML == 'Edit') {
                //При редактировании книги в аттрибут data-edit-id формы присваивается ее идентификатор.
                //Его нужно передать в функцию, чтобы указать какую книгу мы редактируем.
                this.books.editBook(+form.dataset.editId, book);
                this.showBooks();
                action.innerHTML = 'Add';
            }
            form.reset();
            return false;
        }
    }, {
        key: 'edit',
        value: function edit(id) {
            var form = document.forms.book;
            //Аттрибуту data-edit-id формы присваивается идентификатор редактируемой книги.
            form.dataset.editId = id;
            var book = this.books.getBook(id);
            form.name.value = book.name;
            form.author.value = book.author;
            form.year.value = book.year;
            form.pages.value = book.pages;
            //Кнопка Add в режиме редактирования меняет текст на Edit.
            form.button.innerHTML = 'Edit';
        }

        //Метод вызывается всякий раз, когда в списке книг происходят изменения.

    }, {
        key: 'showBooks',
        value: function showBooks() {
            //Удаляем все старые элементы из списка книг.
            var tbody = document.querySelector('tbody');
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            }

            //Проходимся по коллекции книг циклом
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.books.getBooks()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var book = _step.value;

                    var tr = document.createElement('tr');
                    //В аттрибут data-id каждого tr вписываем идентификатор книги
                    tr.dataset.id = book[0];
                    //Создаем tr при помощи данного шаблона
                    tr.innerHTML = '\n                <td>' + book[1].name + '</td>\n                <td>' + book[1].author + '</td>\n                <td>\n                    <button type="button" class="btn btn-xs">Edit</button>\n                    <button type="button" class="btn btn-xs btn-danger">Delete</button>\n                </td>\n            ';
                    tbody.appendChild(tr);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }]);

    return BooksListApp;
}();