import {Book} from './book';
import {BooksRepository} from './books-repository';

export class BooksListApp {
    constructor() {
        //Вся работа с данными идет через отдельный класс BooksRepository.
        this.books = new BooksRepository();

        let table = document.querySelector('table');
        table.onclick = event => this.onBookAction(event);

        let form = document.querySelector('form');
        form.onsubmit = event => this.onFormSubmit(event);

        this.showBooks();
    }

    onBookAction(event) {
        //Используя делегирование обрабатываем нажатие на кнопки в списке книг.
        let target = event.target;
        if (target.tagName == 'BUTTON') {
            //В каждом tr присутствует аттрибут data-id, который соответствует идентификатору книги. Определяем его.
            let elemId = +target.parentElement.parentElement.dataset.id;
            if (target.innerHTML == "Edit") {
                //Вызываем метод, инициирующий редактирование книги
                this.edit(elemId);
            } else if (target.innerHTML == "Delete") {
                this.books.deleteBook(elemId);
                this.showBooks();
            }
        }
    }

    onFormSubmit(event) {
        let action = document.querySelector('#submit');
        let form = document.forms.book;
        let book = new Book(
            form.name.value,
            form.author.value,
            form.year.value,
            form.pages.value);
        if (action.innerHTML == 'Add') {
            this.books.addBook(book);
            this.showBooks();
        } else if (action.innerHTML == 'Edit') {
            //При редактировании книги в аттрибут data-edit-id формы присваивается ее идентификатор.
            //Его нужно передать в функцию, чтобы указать какую книгу мы редактируем.
            this.books.editBook(+form.dataset.editId, book);
            this.showBooks();
            action.innerHTML = 'Add';
        }
        form.reset();
        return false;
    }

    edit(id) {
        let form = document.forms.book;
        //Аттрибуту data-edit-id формы присваивается идентификатор редактируемой книги.
        form.dataset.editId = id;
        let book = this.books.getBook(id);
        form.name.value = book.name;
        form.author.value = book.author;
        form.year.value = book.year;
        form.pages.value = book.pages;
        //Кнопка Add в режиме редактирования меняет текст на Edit.
        form.button.innerHTML = 'Edit';
    }

    //Метод вызывается всякий раз, когда в списке книг происходят изменения.
    showBooks() {
        //Удаляем все старые элементы из списка книг.
        let tbody = document.querySelector('tbody');
        while (tbody.firstChild) {
            tbody.removeChild(tbody.firstChild);
        }

        //Проходимся по коллекции книг циклом
        for (let book of this.books.getBooks()) {
            let tr = document.createElement('tr');
            //В аттрибут data-id каждого tr вписываем идентификатор книги
            tr.dataset.id = book[0];
            //Создаем tr при помощи данного шаблона
            tr.innerHTML = `
                <td>${book[1].name}</td>
                <td>${book[1].author}</td>
                <td>
                    <button type="button" class="btn btn-xs">Edit</button>
                    <button type="button" class="btn btn-xs btn-danger">Delete</button>
                </td>
            `;
            tbody.appendChild(tr);
        }
    }
}
