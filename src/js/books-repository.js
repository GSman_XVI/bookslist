import {Book} from './book';

export class BooksRepository {
    constructor() {
        //Коллекция, в которой будет храниться список книг.
        this.books = new Map();
        this.loadBooks();
    }

    getBooks() {
        return this.books;
    }

    getBook(id) {
        return this.books.get(id);
    }

    addBook(book) {
        //Создаем ключ для новой книги, прибавляя к последнему ключу (если он есть) единицу.
        let lastId = Array.from(this.books.keys()).pop();
        if (isNaN(lastId)) {
            lastId = 0;
        } else {
            lastId++;
        }
        this.books.set(lastId, book);
        this.saveBooks();
    }

    editBook(id, book) {
        this.books.set(id, book);
        this.saveBooks();
    }

    deleteBook(id) {
        this.books.delete(id);
        this.saveBooks();
    }

    saveBooks() {
        //Преобразуем Map в обычный массив для записи в localStorage.
        let data = [];
        for (let book of this.books.values()) {
            data.push(book);
        }
        localStorage.data = JSON.stringify(data);
    }

    loadBooks() {
        //Достаем массив книг из localStorage и записываем их в Map, попутно создавая каждому уникальный ключ.
        if (localStorage.data) {
            let data = JSON.parse(localStorage.data);
            for (let i = 0; i < data.length; i++) {
                this.books.set(i, data[i]);
            }
        }
    }
}
