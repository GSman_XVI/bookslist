export class Book {
    constructor(name, author, year, pages) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.pages = pages;
    }
}
