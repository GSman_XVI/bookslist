# BooksList App #

### Installation ###

```
#!cmd
git clone https://GSman_XVI@bitbucket.org/GSman_XVI/bookslist.git
```

```
#!cmd
cd bookslist
```

```
#!cmd
npm install
```

```
#!cmd
npm install bower -g
```

```
#!cmd
bower install
```

```
#!cmd
npm install gulp -g
```

```
#!cmd
gulp
```

```
#!cmd
Run index.html
```